---
title: "Couldn't download IntelliCode model. Please check your network connectivity or firewall settings."
date: "2019-03-18"
description: "Downloading VS IntelliCode models behind corporate proxy"
tags:
- VS Code
- Intellicode
- Corporate Proxy
---

If you see this error popping up in VS Code, chance are high that the problem is
the corporate proxy you are behind.

There's ana esay way to check:

- Open the output window if it's not already open (SHIFT+CTRL+P *Toggle Output*)
- Select `VS Intellicode` from the select input


{{% figure src="/assets/images/vscode-intellicode-output.png" title="VS Intellicode output" %}}


There you'll see the real error message reported by `Intellicode` itself.

In my case it was `Couldn't reach service Error Message: unable to verify the first certificate`.

Unfortunately `Intellicode` doesn't obey to `VS Code` setting `http.proxyStrictSSL` and tries to
validate the SSL certificate no matter what.

Fortunately the solution is pretty straightforward: instruct the underlying `node` environment to ignore SSL certificate validation using the `NODE_TLS_REJECT_UNAUTHORIZED` environment variable, like this

```bash
$ NODE_TLS_REJECT_UNAUTHORIZED="0" code .
```

Et voilà, enjoy `Intellicode`!

{{% figure src="/assets/images/vscode-intellicode-output-success.png" title="VS Intellicode success" %}}






