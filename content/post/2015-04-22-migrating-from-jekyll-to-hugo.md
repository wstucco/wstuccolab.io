---
title: "Migrating from Jekyll to Hugo"
date: "2015-04-22"
description: I was ok with Jekyll, with Hugo I'm happy.
tags:
  - Jekyll
  - Hugo
  - Static site generators
draft: true
---

This webiste is not run by [Jekyll](http://jekyllrb.com/) anymore, say hello to [Hugo](http://gohugo.io/)