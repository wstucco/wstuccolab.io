---
title: "Phoenix Framework: long running processes"
date: "2016-04-30"
description: "Phoenix is the exciting new kid on the block in the vast world of web frameworks.  
Its roots are in Rails, with the bonus of the performances of a compiled language.<br>  
This time we're going to explore the assets pipeline."
source_name: MIKAMAYHEM
source_url: http://dev.mikamai.com/post/143627439174/phoenix-framework-the-assets-pipeline
tags:
- Elixir
- Phoenix Framework
- Rails alternatives
- Redis alternatives
- Running batch jobs
- Phoenix long running processes
- Phoenix to the basics and beyond
draft: true
---

* [Part 1: To the basics and beyond](/2016/02/04/phoenix-framework-to-the-basics-and-beyond.html#content)
* [Part 2: The assets pipeline](http://dev.mikamai.com/post/143627439174/phoenix-framework-the-assets-pipeline)
* Part 3: Long running processes

## Long running processes, why and when

When running a task at predefined intervals (check something every N minutes) or when dealing with async queues (send a welcom email to a new user)
The most common solution to this problem is running a job scheduler service, usually coupled with a queue manager.  
In the Ruby world, for example, you'll most likely find Sidekiq+Redis.   

## Phoenix Applications

Phoenix applications are Supervised Elixir applications.   
They are continuously running and being supervised by Elixir.   

#### Defaults


#### Plugins


### Tool belt


### There's more than a way

