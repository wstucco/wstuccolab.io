---
title: "New website layout"
date: "2016-05-01"
description: "Time to grow up, a new layout for a new adventure. Goodbye to the old
one that served me so well for so long"
tags:
- Massimo Ronca
- Layout
- Branding
---

This is my first major redesign.
I've been tweaking the old layout for a long time now, but the more time went by,
the more I felt it wasn't actual anymore, I changed my brand identity and so must the layout.
This new one has been a work in progress for some months and now I feel it is ready
to be put online for testing.
The main ideas behind it were readability - through a monospace font, the same used
  for code - and simplicity, I just wanted the content to stand out and nothing else.

The beauty of using a static site generator is that you can commit every little change
into a Git repository and watch it change over time.
You'll never lose a single change and you'll never forget when and why you did
that change.
One could almost make a short movie after a few years, just like those people taking
a shot a day of their attempts to grow a beard.
I will try to post some screenshot of the updates that lead here.
So long old layout, welcome new one.

Thanks to [Riseabove](http://www.riseabove.it/) for the logo.

{{% figure src="/assets/images/old-layout.png" title="Previous layout" %}}

**UPDATE 2017-01-28**: I'm so fond on Solarized theme that I reverted back the site
colors to match Solarized light ones. Gray was just boring.

{{% figure src="/assets/images/old-layout-2.png" title="Gray layout" %}}
