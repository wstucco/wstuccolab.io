---
title: "Could not start application eldap: could not find application file: eldap.app"
date: "2018-04-10"
description: "One of the Erlang's strongest selling point is to come packaged with everything you need. But sometimes you have to find it."
tags:
- Elixir tips'n'tricks
- Elixir ldap
- Erlang eldap
- Erlang
- Elixir

images:
  - /assets/images/elixir_eldap.png
---

This story begins with a quest, the quest to write an SSO service for my company's AD, allowing everyone to use the domain's credentials and be done with auth(entication) & auth(orization) forever and ever.

One day I'll tell you the story of how the quest ended, but today is the day I'll tell you how to work with `:eldap` with Erlan/Elixir on Debian and be happy.

One of the great benefits of small packages is the ability to install only what you need and nothing else, avoiding the clutter associated with big, one-fits-all bundles.

If you search for Erlang on Debian's apt you get a long response back.

```sh
$ sudo apt-cache search --names-only ^erlang-. | wc -l
120
```

That's a lot of packages!

TBH, my Erlang installation is fairly standard, I just installed `esl-erlang`, which contains the the Erlang/OTP platform and all of its applications, and called it a day.  

Little did I know that Ldap, and specifically `eldap` Erlang package, is not included in the bundle...

Long story short: if you're on Debian (but probably other distros are affected as well) and you see this error message **`Could not start application eldap: could not find application file: eldap.app`** you can fix it with a simple

```sh
$ sudo apt install erlang-ldap
```



