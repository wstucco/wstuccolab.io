---
title: "Fixing SELinux and passwordless SSH authentication"
date: "2017-03-14"
description: "You tried setting up public key, passwordless authentication on your
Linux box, but sshd keeps asking for a password and ignores all the keys you send.<br>
You check the logs and there's a message like this one
<code>type=AVC msg=audit(1392479922.440:24765601): avc:  denied  { read } for  pid=13960 comm=\"sshd\" name=\"authorized_keys\" dev=dm-0 ino=786507 scontext=unconfined_u:system_r:sshd_t:s0-s0:c0.c1023 tcontext=unconfined_u:object_r:httpd_sys_content_t:s0 tclass=file</code>"
tags:
- SELinux
- SSH public key authentication
- SSH public key authentication SELinux
- SSH public key authentication RHEL CentOS
- SSH passwordless
- SSH passwordless SELinux

---

You tried setting up public key, passwordless authentication on your
Linux box, but sshd keeps asking for a password and ignores all the keys you send.
You are probably running RHEL or CentOS and SELinux.

The first thing you might try is fixing the permissions, `700` for `~/.ssh`, 
`600` or `644` for `~/.ssh/authorized_keys`.  
Still doesn't work.

You start investigating the issue and in the logs (probably `/var/log/secure`) you find a message like this one  

> type=AVC msg=audit(1392479922.440:24765601): avc:  denied  { read } for  pid=13960 comm=\"sshd\" name=\"authorized_keys\" dev=dm-0 ino=786507 scontext=unconfined_u:system_r:sshd_t:s0-s0:c0.c1023 tcontext=unconfined_u:object_r:httpd_sys_content_t:s0 tclass=file

Bingo!  
It's SELinux blocking the system from reading the files.

If you lookup on Google everyone tells you to use `restorecon`, something like `restorecon -R -v ~/.ssh`.  
You try different combinations of the command, with variuos targets `restorecon -R -v ~/.ssh/authorized_keys` or  
`restorecon -R -v ~/`, but it doesn't work, it still asks for a password.   

You've been trying to fix this issue for hours now, you're starting to get desperate and
maybe a little angry.   
I'll tell you the solution that worked for me:  
run `ls -laZ ~/.ssh` anche check the output, it should be something similar to this

```bash
drwx------. user group system_u:object_r:default_t:s0` 
```

Wait, wait, wait!   
What's that `-Z`?  
According to man it's  

```bash
-Z, --context
      print any security context of each file

```

You need to change the security context to `system_u:object_r:usr_t:s0`.  
How?  
Just run

```bash
chcon -R -v system_u:object_r:usr_t:s0 ~/.ssh/
```

Et voilat, your public key, passwordless authentication is now online.  