---
title: Disable captive network support in OS X
date: "2015-05-20"
description: "When your Apple device connects to an access point, it tries to download a file to test network connectivity.
If the OS can’t download the file, it assumes you’re behind a Captive Portal, basically a web form that requires you to
authenticate to the network in various ways. This can be very annoying sometimes."
tags:
- OS X
- captive network support
- captive portals
- WI-FI authorization
---

When your Apple device connects to an access point and the configuration looks right
(a DNS and an IP address are assigned to the device), it tries to download a file
to test network connectivity.
Specifically this file is http://www.apple.com/library/test/success.html for everything
below Yosemite and http://captive.apple.com/hotspot-detect.html for Yosemite and above.

If the OS can't download the file, it assumes you're behind a [Captive Portal](https://en.wikipedia.org/wiki/Captive_portal),
basically a web form that requires you to authenticate to the network in various ways.  
You've probably seen them in airports or public Wi-fi networks.  
This can be very annoying sometimes.

To disable this default behaviour, you can set this preference:

`sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.captive.control Active -boolean false`

to completely delete it

`sudo defaults delete /Library/Preferences/SystemConfiguration/com.apple.captive.control Active`

You can still logon by using your browser and if it doesn't work, you can mannually
launch the app located at `/System/Library/CoreServices/Captive Network Assistant.app`