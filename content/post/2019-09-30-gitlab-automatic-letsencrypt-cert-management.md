---
title: "Gitlab + Letsencrypt automatic certificate management"
date: "2019-09-30"
description: ""
tags:
- Gitlab
- Letsencrypt
- HTTPS/SSL
- Gitlab Letsencrypt automation

---

Gitlab has finally introduce automatic certificate management for Gitlab pages that 
use Letsencrypt certificates. 

This was a long awaited feature and it looks they nailed it, it's just a simple 
ON/OFF switch in the SSL settings of your domain. 


Kudos to the Gitlab team for making an already great product better with every release.

Details of the inner working can be found at [GitLab Pages integration with Let's Encrypt](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html#enabling-lets-encrypt-integration-for-your-custom-domain)

{{% figure src="/assets/images/gitlab-letsencrypt_auto.png" title="Gitlab is great" %}}